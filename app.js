var async = require('async');
var cluster = require('cluster');
var Crawler = require('./crawlerMonster.js');
var numCPUs = 2;

if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
        console.log("forking");
    }
} else {
    async.forever(function(cb){
	    Crawler.crawl(function(){
	        process.nextTick(function(){
	            cb(null);
	        });
	    });
	});
}

