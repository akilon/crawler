var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var db = require('mysql');
var util = require('./utils.js');

var Crawler = (function() {
    var config = {
        "mysql" : {
            "host" : "localhost",
            "port" : 3306,
            "user" : "root",
            "password" : "root@321",
            "database" : "jobcrawler"
        },
        "baseSite" : "http://jobsearch.monster.com.my/jobresults/javascript-jobs.html"
    };
    var conn = db.createConnection(config.mysql);

    var crawl = function(callback){
        conn.query('SELECT * FROM search_queue ORDER BY RAND() LIMIT 0,1', function(e,r){
            if (e) throw e;

            var url = r.length > 0 ? r[0].url : config.baseSite;
            var queue = r.length > 0 ? r[0] : {};
            request(url,function(err,res,body){
                if (err) throw err;
                console.log(queue)
                if (queue) {
                    queue.body = body;
                    /*conn.query('DELETE FROM queue WHERE id = ?',[queue.id],function(){
                        callback();
                    }) */
                    processData(queue)
                    callback()
                } else {                    
                    processData({ "body" : body, "url" : url, "from2" : "" })
                    callback()
                }
            })
        })
    }

    var processData = function(queue){
        var $ = cheerio.load(queue.body);
        var title = $('head title').text();
        var links = $('a');
        var count = 0;

        async.map(links.map(function(){
            var href = $(this).attr('href');
            count++;

            if(href && !(/^#(\w)+/.test(href)) && !util.imageRegexp.test(href)){
                //if(util.isExternal(href)){
                    if (href.indexOf("jobsearch.monster.com.my/jobresults/javascript-jobs.html?n=") > 0) {
                        conn.query('INSERT INTO search_queue SET url = ? ON DUPLICATE KEY UPDATE url = ?', [href, href]);
                    }
                    /*
                    if (href.indexOf("jobs.monster.com.my/details") > 0 || href.indexOf("jobsearch.monster.com.my/jobresults/javascript-jobs.html?n=") > 0) {
                        conn.query('INSERT INTO queue SET url = '+conn.escape(href)+', from2 = '+conn.escape(queue.from2));
                    }
                    */
                //} else {
                    //self.conn.query('INSERT INTO `queue` SET `id` = \''+util.id()+'\', `url` = '+self.conn.escape(util.resolveRelativeURL(href,self._url))+', `from2` = '+self.conn.escape(from));
                    //return 'INSERT INTO `queue` SET `id` = \''+util.id()+'\', `url` = '+self.conn.escape(util.resolveRelativeURL(href,self._url))+', `from2` = '+self.conn.escape(from);
                //}
            }

        }).filter(function(el){
            
        }),conn.query.bind(conn),function(e,result){
            if(e){
                console.log('Error writing queue.');
                console.log(e);
            }
        });
    }

    return {
        crawl
    }

})();


module.exports = Crawler;